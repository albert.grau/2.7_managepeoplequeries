/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.copernic.managePeople.dao;

import com.copernic.managePeople.domain.Person;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonDAO extends JpaRepository<Person, Long> {

        @Query("SELECT p FROM Person p WHERE p.email LIKE '%@gmail.com'")
    List<Person> findEmailsEndingWithGmail();
    
    
}

