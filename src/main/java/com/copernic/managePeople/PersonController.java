/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.copernic.managePeople.controller;

import com.copernic.managePeople.domain.Person;
import com.copernic.managePeople.services.PersonService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

/**
 *
 * @author Albert Grau
 */
@Controller
public class PersonController {

    @Autowired
    private PersonService personService;

    @GetMapping("/delete/{id}")
    public String showDeleteConfirmation(@PathVariable Long id, Model model) {
        Person person = personService.getPersonById(id);
        model.addAttribute("person", person);
        return "person-delete-confirmation";
    }

    @GetMapping("/deleteOK/{id}")
    public String deletePerson(Person person) {
        personService.deletePerson(person.getId());
        return "redirect:/persons";
    }

    @GetMapping("/update/{id}")
    public String update(Person person, Model model) {
        person = personService.findPerson(person);
        model.addAttribute("person", person);
        return "person-form";
    }

    @GetMapping("/person")
    public String showForm(Person person) {
        return "person-form";
    }

    @PostMapping("/person")
    public String submitForm(Person person, Model model) {

        // Save the person object to the database
        personService.savePerson(person);
        return "redirect:/persons";
    }

    @GetMapping({"/persons", "/"})
    public String listPersons(Model model) {
        List<Person> persons = personService.getAllPersons();
        model.addAttribute("persons", persons);
        return "person-list";
    }

    @GetMapping("/emails-with-gmail")
    public String getEmailsEndingWithGmail(Model model) {
        List<Person> emails = personService.getEmailsEndingWithGmail();
        model.addAttribute("persons", emails);
        return "person-list";
    }

}
