/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.copernic.managePeople.services;

import com.copernic.managePeople.domain.Person;
import java.util.List;

/**
 *
 * @author Albert Grau
 */
public interface PersonService {

    Person savePerson(Person person);

    List<Person> getAllPersons();

    Person findPerson(Person person);

    Person getPersonById(Long id);

    void deletePerson(Long id);

    List<Person> getEmailsEndingWithGmail();

}
