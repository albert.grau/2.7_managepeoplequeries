/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.copernic.managePeople.services;

/**
 *
 * @author Albert Grau
 */
import com.copernic.managePeople.dao.PersonDAO;
import com.copernic.managePeople.domain.Person;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class PersonServiceImpl implements PersonService {

    @Autowired
    private PersonDAO personDAO;

    @Override
    @Transactional
    public Person savePerson(Person person) {
        return personDAO.save(person);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Person> getAllPersons() {
        return personDAO.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Person findPerson(Person person) {
        return personDAO.findById(person.getId()).orElse(null);

    }

    @Override
    @Transactional(readOnly = true)
    public Person getPersonById(Long id) {
        return personDAO.findById(id).orElse(null);

    }

    @Transactional
    public void deletePerson(Long id) {
        Person person = personDAO.findById(id).orElse(null);
        personDAO.delete(person);
    }

    @Transactional(readOnly = true)
    public List<Person> getEmailsEndingWithGmail() {
        return personDAO.findEmailsEndingWithGmail();
    }
}
